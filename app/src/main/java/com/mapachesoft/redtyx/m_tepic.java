package com.mapachesoft.redtyx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class m_tepic extends AppCompatActivity {

    ImageView saltocentro;
    ImageView saltohcasas;
    ImageView saltocomerciantes;
    ImageView saltoconchas;
    ImageView saltofresnos;
    ImageView siguiente;
    TextView cerrar;
    ImageView menu_ini;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_m_tepic);

        saltocentro=(ImageView)findViewById(R.id.btncentro);
        saltocentro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent scntr=new Intent(getApplicationContext(),rt_centro.class);
                startActivity(scntr);
            }
        });


        saltohcasas=(ImageView)findViewById(R.id.btncasas);
        saltohcasas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shc=new Intent(getApplicationContext(),rt_hcasas.class);
                startActivity(shc);
            }
        });


        saltocomerciantes=(ImageView)findViewById(R.id.btncomerciantes);
        saltocomerciantes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cm=new Intent(getApplicationContext(),rt_comerciantes.class);
                startActivity(cm);
            }
        });

        saltoconchas=(ImageView)findViewById(R.id.btnconchas);
        saltoconchas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent con=new Intent(getApplicationContext(),rt_conchas.class);
                startActivity(con);
            }
        });

        saltofresnos=(ImageView)findViewById(R.id.btnfresnos);
        saltofresnos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent free=new Intent(getApplicationContext(),rt_fresnos.class);
                startActivity(free);
            }
        });
        siguiente=(ImageView) findViewById(R.id.next);
        siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nex=new Intent(getApplicationContext(),m_tepic_2.class);
                startActivity(nex);
                Toast.makeText(m_tepic.this, "Siguiente" , Toast.LENGTH_LONG).show();
            }
        });
        menu_ini=(ImageView) findViewById(R.id.menu_image);
        menu_ini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent men= new Intent(getApplicationContext(),Amenu.class);
                startActivity(men);
                Toast.makeText(m_tepic.this, "Menu principal" , Toast.LENGTH_LONG).show();
            }
        });
    }
}



