package com.mapachesoft.redtyx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Amenu extends AppCompatActivity {

    ImageView tepic;
    ImageView xalisco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amenu);

        tepic=(ImageView)findViewById(R.id.imtepic);
        tepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tepicv=new Intent(getApplicationContext(),m_tepic.class);
                startActivity(tepicv);
            }
        });

        xalisco=(ImageView)findViewById(R.id.imxalisco);
        xalisco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent xaliscov=new Intent(getApplicationContext(),m_xalisco.class);
                startActivity(xaliscov);
            }
        });

    }
}
