package com.mapachesoft.redtyx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class m_xalisco extends AppCompatActivity {

    ImageView progresox;
    ImageView nuevox;
    ImageView millanx;
    ImageView centrox;
    ImageView atrasx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_m_xalisco);

        progresox=(ImageView)findViewById(R.id.btnxnuevo);
        progresox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent prg=new Intent(getApplicationContext(),rx_nuevo.class);
                startActivity(prg);
            }
        });
        nuevox=(ImageView)findViewById(R.id.btnxpueblo);
        nuevox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nu=new Intent(getApplicationContext(),rx_pueblo.class);
                startActivity(nu);
            }
        });
        millanx=(ImageView)findViewById(R.id.btnxramos);
        millanx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mil=new Intent(getApplicationContext(),rx_ramos.class);
                startActivity(mil);
            }
        });
        centrox=(ImageView)findViewById(R.id.btnxcentro);
        centrox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cx=new Intent(getApplicationContext(),rx_centro.class);
                startActivity(cx);
            }
        });
        atrasx=(ImageView) findViewById(R.id.atras_0);
        atrasx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ax=new Intent(getApplicationContext(),Amenu.class);
                startActivity(ax);
                Toast.makeText(m_xalisco.this, " MENU PRINCIPAL" , Toast.LENGTH_LONG).show();
            }
        });
    }
}


