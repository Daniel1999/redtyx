package com.mapachesoft.redtyx;

import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import OpenHelper.SQLite_OpenHelper;

public class alogin extends AppCompatActivity {

    ImageView Registros;
    ImageView Ingresos;

    SQLite_OpenHelper helper=new SQLite_OpenHelper(this,"BD1",null,1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alogin);

        Registros=(ImageView)findViewById(R.id.btnregistrar);

        Registros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ar=new Intent(getApplicationContext(),Aregistros.class);
                startActivity(ar);
            }
        });

        Ingresos=(ImageView) findViewById(R.id.btningresar);
        Ingresos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txtusub=(EditText)findViewById(R.id.etusuario);
                EditText txtpasb=(EditText)findViewById(R.id.etcontrasena);

                try {
                    Cursor cursor = helper.ConsultarUsuario(txtusub.getText().toString(), txtpasb.getText().toString());

                    if (cursor.getCount()>0){
                        Intent ing=new Intent(getApplicationContext(),Amenu.class);
                        startActivity(ing);
                    }else {
                        Toast.makeText(getApplicationContext(),"Usuario y/o Contraseña Incorrectos",Toast.LENGTH_LONG).show();
                    }
                    txtusub.setText("");
                    txtpasb.setText("");
                    txtusub.findFocus();

                }catch (SQLException e){
                    e.printStackTrace();
                }
            }
        });

    }
}
