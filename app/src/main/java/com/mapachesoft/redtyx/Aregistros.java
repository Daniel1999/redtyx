package com.mapachesoft.redtyx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import OpenHelper.SQLite_OpenHelper;

public class Aregistros extends AppCompatActivity {

    ImageView btngrabar;
    EditText txtnom,txtedad,txtcor,txtpass;

    SQLite_OpenHelper helper=new SQLite_OpenHelper(this,"BD1",null,1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aregistros);

        btngrabar=(ImageView) findViewById(R.id.btnregistrarusuario);
        txtnom=(EditText)findViewById(R.id.etnombreusuario);
        txtedad=(EditText)findViewById(R.id.etedad);
        txtcor=(EditText)findViewById(R.id.etcorreo);
        txtpass=(EditText)findViewById(R.id.etpassusuario);

        btngrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helper.abrir();
                helper.insertarReg(String.valueOf(txtnom.getText()),String.valueOf(txtedad.getText()),String.valueOf(txtcor.getText()),String.valueOf(txtpass.getText()));
                helper.cerrar();

                Intent i=new Intent(getApplicationContext(),alogin.class);
                startActivity(i);
            }
        });


    }
}
