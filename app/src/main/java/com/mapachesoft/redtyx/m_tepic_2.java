package com.mapachesoft.redtyx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class m_tepic_2 extends AppCompatActivity {

    ImageView sjacarandas;
    ImageView sjardines;
    ImageView smiguel;
    ImageView svalle;
    ImageView svistas;
    ImageView anterior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_m_tepic_2);

        sjacarandas=(ImageView)findViewById(R.id.btnjacarandas);
        sjacarandas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent jaka=new Intent(getApplicationContext(),rt_jacarandas.class);
                startActivity(jaka);
            }
        });
        sjardines=(ImageView)findViewById(R.id.btnjardines);
        sjardines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent jar=new Intent(getApplicationContext(),rt_jardines.class);
                startActivity(jar);
            }
        });
        smiguel=(ImageView)findViewById(R.id.btnmiguel);
        smiguel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent miky=new Intent(getApplicationContext(),rt_miguel.class);
                startActivity(miky);
            }
        });

        svalle=(ImageView)findViewById(R.id.btnlomas);
        svalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ciu=new Intent(getApplicationContext(),rt_valle.class);
                startActivity(ciu);
            }
        });

        svistas=(ImageView)findViewById(R.id.btnvistas);
        svistas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vis=new Intent(getApplicationContext(),rt_vistas.class);
                startActivity(vis);
            }
        });

        anterior=(ImageView) findViewById(R.id.anterior);
        anterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back=new Intent(getApplicationContext(),m_tepic.class);
                startActivity(back);
            }
        });
    }
}
