package OpenHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLite_OpenHelper extends SQLiteOpenHelper {
    /************************************************************************************/
    public SQLite_OpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context,name,factory,version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query="create table usuarios (_ID integer primary key autoincrement,Nombre text,Edad text,Correo text,Password text)";
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    /*Abrir*/
    public void abrir(){
        this.getWritableDatabase();
    }
    //Cerrar
    public void cerrar(){
        this.close();
    }
    //Insertar
    public void insertarReg(String nom,String edad,String cor,String pass){
        ContentValues valores=new ContentValues();
        valores.put("Nombre",nom);
        valores.put("Edad",edad);
        valores.put("Correo",cor);
        valores.put("Password",pass);

        this.getWritableDatabase().insert("usuarios",null,valores);


    }

    //Validacion
    public Cursor ConsultarUsuario(String usuv, String pasv) throws SQLException {
        Cursor mcursor=null;
        mcursor=this.getReadableDatabase().query("usuarios",new String[]{"_ID","Nombre","Edad","Correo","Password"},"Correo like '"+usuv+"' and Password like '"+pasv+"'",null,null,null,null);
        return mcursor;
    }



}
